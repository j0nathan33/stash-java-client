package it.com.atlasssian.stash.rest.client.tests;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.httpclient.HttpClientConfig;
import com.atlassian.stash.rest.client.httpclient.HttpClientStashClientFactoryImpl;
import org.junit.runner.RunWith;

import java.net.URI;

@RunWith(AtlassianPluginsTestRunner.class)
public class HttpClientStashClientWiredTest extends StashClientIntegrationTestBase {

    @Override
    protected StashClient createStashClient(String stashUsername, String stashPassword) throws Exception {
        HttpClientStashClientFactoryImpl stashClientFactory = new HttpClientStashClientFactoryImpl();
        return stashClientFactory.getStashClient(new HttpClientConfig(URI.create(TestUtil.getProductUrl("stash.or.bitbucket")).toURL(), stashUsername, stashPassword));
    }

}
