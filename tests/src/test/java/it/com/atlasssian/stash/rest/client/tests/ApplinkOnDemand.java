package it.com.atlasssian.stash.rest.client.tests;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.ReciprocalActionException;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.List;

public class ApplinkOnDemand {
    static Logger log = LoggerFactory.getLogger(ApplinkOnDemand.class);

    private final MutatingApplicationLinkService linkService;
    private final TypeAccessor typeAccessor;
    private final ManifestRetriever manifestRetriever;

    private List<ApplicationId> createdLinkIds = Lists.newArrayList();

    public ApplinkOnDemand(MutatingApplicationLinkService linkService, TypeAccessor typeAccessor, ManifestRetriever manifestRetriever) {
        this.linkService = linkService;
        this.typeAccessor = typeAccessor;
        this.manifestRetriever = manifestRetriever;
    }

    public ApplicationLink createTrustedAppsLink(String baseUrl, String name, String remoteUsername, String remotePassword)
            throws ReciprocalActionException, ManifestNotFoundException, AuthenticationConfigurationException {
        final URI uri = URI.create(baseUrl);
        final Manifest manifest = manifestRetriever.getManifest(uri);
        final ApplicationType type = typeAccessor.loadApplicationType(manifest.getTypeId());

        linkService.createReciprocalLink(uri, null, remoteUsername, remotePassword);
        ApplicationLink link = linkService.createApplicationLink(type, ApplicationLinkDetails.builder()
                .displayUrl(uri)
                .rpcUrl(uri)
                .name(name)
                .isPrimary(true)
                .build());
        createdLinkIds.add(link.getId());
        linkService.configureAuthenticationForApplicationLink(link, new ApplinkAuthenticationScenario(true, true),
                remoteUsername, remotePassword);

        return link;
    }

    public ApplicationLink get() {
        try {
            return (!createdLinkIds.isEmpty() ? linkService.getApplicationLink(createdLinkIds.get(0)) : null);
        } catch (TypeNotInstalledException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteCreated() throws TypeNotInstalledException {
        for (ApplicationId linkId : createdLinkIds) {
            MutableApplicationLink link = linkService.getApplicationLink(linkId);
            delete(link);
        }
        createdLinkIds.clear();
    }

    public void deleteByType(Class<? extends ApplicationType> type) throws TypeNotInstalledException {
        Iterable<ApplicationLink> links = linkService.getApplicationLinks(type);
        for (ApplicationLink link : links) {
            delete(link);
        }
    }

    public Iterable<ApplicationLink> listByType(Class<? extends ApplicationType> type) throws TypeNotInstalledException {
        return linkService.getApplicationLinks(type);
    }

    public void delete(ApplicationLink applink) {
        try {
            linkService.deleteReciprocatedApplicationLink(applink);
        } catch (ReciprocalActionException | CredentialsRequiredException e) {
            log.warn("Cannot removed reciprocal applink", e);
        }
        linkService.deleteApplicationLink(applink);
    }

    private static class ApplinkAuthenticationScenario implements AuthenticationScenario {
        private final boolean commonUserBase;
        private final boolean trusted;

        private ApplinkAuthenticationScenario(boolean commonUserBase, boolean trusted) {
            this.commonUserBase = commonUserBase;
            this.trusted = trusted;
        }

        @Override
        public boolean isCommonUserBase() {
            return commonUserBase;
        }

        @Override
        public boolean isTrusted() {
            return trusted;
        }
    }
}
