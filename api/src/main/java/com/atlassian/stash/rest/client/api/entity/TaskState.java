package com.atlassian.stash.rest.client.api.entity;

public enum TaskState {
    OPEN,
    RESOLVED
}