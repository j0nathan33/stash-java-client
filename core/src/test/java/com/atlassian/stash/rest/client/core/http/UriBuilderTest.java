package com.atlassian.stash.rest.client.core.http;

import org.junit.Test;

import java.net.URI;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class UriBuilderTest {
    @Test
    public void forPath_noArgs() throws Exception {
        final String uri = UriBuilder.forPath("/some/path/it/is").build();

        assertThat(uri, is("/some/path/it/is"));
        assertThat(new URI(uri).getPath(), is("/some/path/it/is"));
        assertThat(new URI(uri).getQuery(), is(nullValue()));
    }

    @Test
    public void forPath_someArgs() throws Exception {
        final String uri = UriBuilder.forPath("/questionMark/%s" +
                        "/ampersand/%s" +
                        "/space/%s" +
                        "/goingWild/%s",
                "?",
                "&",
                " ",
                "!@#$%^&*()<>:'[]{}")
                .build();

        assertThat(uri, is("/questionMark/%3F" +
                "/ampersand/%26" +
                "/space/%20" +
                "/goingWild/%21%40%23%24%25%5E%26*%28%29%3C%3E%3A%27%5B%5D%7B%7D"));
        assertThat(new URI(uri).getPath(), is("/questionMark/?" +
                "/ampersand/&" +
                "/space/ " +
                "/goingWild/!@#$%^&*()<>:'[]{}"));
        assertThat(new URI(uri).getQuery(), is(nullValue()));

    }

    @Test
    public void forPath_withQueryParams() throws Exception {
        final String uri = UriBuilder.forPath("/path")
                .addQueryParam("<name?&>", "<value?&>")
                .addQueryParam("abc", "def")
                .addQueryParam("param with spaces", "value with spaces")
                .addQueryParam("paramsWithNullValuesIgnored", null)
                .build();

        assertThat(uri, is("/path?" +
                "%3Cname%3F%26%3E=%3Cvalue%3F%26%3E" +
                "&abc=def" +
                "&param%20with%20spaces=value%20with%20spaces"));
        assertThat(new URI(uri).getPath(), is("/path"));
        assertThat(new URI(uri).getQuery(), is("<name?&>=<value?&>&abc=def&param with spaces=value with spaces"));
    }

    @Test
    public void forPath_withMapper() throws Exception {
        final String uri = UriBuilder.forPath("/path")
                .encode("<testOnly>", (uriBuilder, s) -> uriBuilder.addQueryParam("param1", s))
                .encode(null, (uriBuilder, s) -> uriBuilder.addQueryParam("param2", "value2"))
                .build();

        assertThat(uri, is("/path?param1=%3CtestOnly%3E"));
        assertThat(new URI(uri).getPath(), is("/path"));
        assertThat(new URI(uri).getQuery(), is("param1=<testOnly>"));
    }
}