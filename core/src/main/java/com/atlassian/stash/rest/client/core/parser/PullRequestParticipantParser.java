package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.PullRequestParticipant;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;

public class PullRequestParticipantParser implements Function<JsonElement, PullRequestParticipant> {
    @Override
    public PullRequestParticipant apply(@Nullable JsonElement json) {
        final JsonObject jsonObject = Preconditions.checkNotNull(json, "json").getAsJsonObject();

        final JsonObject user = Preconditions.checkNotNull(jsonObject.getAsJsonObject("user"), "user");

        final String name = user.get("name").getAsString();
        final String displayName = user.get("displayName").getAsString();
        final String slug = user.get("slug").getAsString();
        final String avatarUrl = user.has("avatarUrl") ? user.get("avatarUrl").getAsString() : null;
        final boolean approved = jsonObject.get("approved").getAsBoolean();
        final String status = jsonObject.has("status") ? jsonObject.get("status").getAsString() : null;
        final String role = jsonObject.get("role").getAsString();

        return new PullRequestParticipant(name, displayName, slug, avatarUrl, approved, status, role);
    }
}
