package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Objects;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;
import java.util.function.Predicate;

public class ParserUtil {
    @Nullable
    public static String getOptionalJsonString(JsonObject json, String name) {
        return getOptionalJsonString(json, name, null);
    }

    @Nullable
    public static String getOptionalJsonString(JsonObject json, String name, String def) {
        JsonElement element = json.get(name);
        if (element != null) {
            if (element.isJsonNull()) {
                return null;
            } else {
                return element.getAsString();
            }
        }
        return def;
    }

    public static Function<Link, String> linkToHref() {
        return LINK_TO_HREF;
    }

    public static Predicate<Link> isLinkName(final String name) {
        return input -> Objects.equal(input.getName(), name);
    }

    public static Predicate<Link> isHttpLink() {
        return IS_HTTP_LINK;
    }

    public static Predicate<Link> isSshLink() {
        return IS_SSH_LINK;
    }

    private static final Function<Link, String> LINK_TO_HREF = input -> input.getHref();
    private static final Predicate<Link> IS_HTTP_LINK = isLinkName("http");
    private static final Predicate<Link> IS_SSH_LINK = isLinkName("ssh");

}
