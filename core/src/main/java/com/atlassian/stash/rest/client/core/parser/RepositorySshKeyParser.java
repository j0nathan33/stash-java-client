package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.Parsers.repositoryParser;

class RepositorySshKeyParser implements Function<JsonElement, RepositorySshKey> {

    @Override
    public RepositorySshKey apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonObject repositoryObject = jsonObject.getAsJsonObject("repository");
        Repository repository = repositoryParser().apply(repositoryObject);

        JsonObject key = jsonObject.getAsJsonObject("key");

        return new RepositorySshKey(
                key.get("id").getAsLong(),
                key.get("text").getAsString(),
                key.get("label").getAsString(),
                repository,
                jsonObject.get("permission").getAsString()
        );
    }

}

